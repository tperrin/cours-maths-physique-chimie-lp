---
author: Votre nom
title: Automatismes 
---

## I. Equations du permier degré :

!!! info "A savoir"

    Exercices d'automatismes à faire en autonomie.
    

??? note "Répondre aux questions"

    Pour répondre aux questions, écrire dans les encadrés prévus pour cela.


??? tip "Astuce"

    Les solutions apparaissent en cliquant dessus.



### 1. Transformer le problème en une équation

Texte 1.1

### 2. Résoudre une équation

Texte 1.2

## II. Inéquation du premier degré :

texte 1

### 1. Transformer le problème en une inéquation

Texte 2.1

### 2. Résoudre une inéquation

Texte 2.2
